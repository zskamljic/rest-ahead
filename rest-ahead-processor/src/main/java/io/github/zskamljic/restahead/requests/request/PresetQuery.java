package io.github.zskamljic.restahead.requests.request;

/**
 * Preset query information.
 *
 * @param name  the query parameter name
 * @param value the value for the field
 */
public record PresetQuery(String name, String value) {
}
